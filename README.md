# mongo seed #

Runs a seeded mongodb docker container.
Useful for a detached development environment.

### Steps

* Put json files in ./data folder
* Edit docker-compose ( change mongodb version, database and user credentials as your need )
* Run docker-compose up -d

